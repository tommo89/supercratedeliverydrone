
vertexShader = "
	uniform mat4 WORLDVIEWPROJ;
	attribute vec4 POSITION;
	attribute vec2 TEXCOORD_0;

	varying vec2 uv;

	void main()
	{
		// Transforming The Vertex
	    gl_Position = WORLDVIEWPROJ * POSITION;

	    uv = TEXCOORD_0;
	}
"

fragmentShader = "
	#version 120

	varying vec2 uv;

	uniform vec3 VIEW_DIRECTION;
	uniform mat4 WORLD;
	uniform vec4 OBJECT_COLOR;

	const vec3 sunlightDirectionWorld = vec3(0,-1,-1);

	uniform sampler2D TEXTURE_0;
	uniform sampler2D TEXTURE_1;

	void main()
	{
		vec4 diffuse = texture2D( TEXTURE_0, uv );
		vec4 normal = texture2D( TEXTURE_1, uv ).xzyw;
		normal.w = 0;

		normal = normalize(WORLD * normal);

		float I = max(0, dot(normalize(-sunlightDirectionWorld), normal.xyz));

		gl_FragColor = diffuse * OBJECT_COLOR;
		gl_FragColor.a = diffuse.a;
	}
"