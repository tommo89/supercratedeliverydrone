#ifndef Cloud_h__
#define Cloud_h__

#include "common_header.h"

#include "Level.h"

namespace Drone
{
	class Cloud : 
		public Sprite
	{
	public:

		Cloud(Object* parent, const Vector& pos) : 
			Sprite(parent, pos, "cloud", -1, true)
		{
			_changeLayer(pos);
		}

		virtual void onAction(float dt) override {

			Vector diff = getGameState()->getViewport()->position - position;
			if (diff.length() > 15)
				_changeLayer(position + diff * 1.8);

			Sprite::onAction(dt);

		}

	protected:
		void _changeLayer(const Vector& pos) {

			getGameState()->removeChild(this);

			static const int cloudLayer [] = {
				LL_CLOUDS, LL_CLOUDS_MIDDLE, LL_CLOUDS_FRONT
			};

			int layer = (int) Math::rangeRandom(0, 3);

			if (pos.y < 6)
				layer = 0;

			pixelScale = 1.5 - ((2-layer) / 2.f);
			color.a = pixelScale.x * 0.7;

			speed.x = 0.2 * pixelScale.x;

			//select a random frame
			setFrame((int)Math::rangeRandom(0, 4));

			getGameState()->addChild(this, cloudLayer[layer]);

			position = pos;
		}
	private:
	};
}

#endif // Cloud_h__
