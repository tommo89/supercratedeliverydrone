#ifndef Crate_h__
#define Crate_h__

#include "common_header.h"

#include "Physthing.h"

#define CRATE_SIDE 0.5f
#define CRATE_DENSITY 0.01f
#define CRATE_FRICTION 0.7f

namespace Drone 
{
	class Crate :
		public Renderable,
		public PhysThing 
	{
	public:

		Crate( Dojo::Object* parent, const Vector& pos ) :
			Renderable(parent, pos, "texturedQuad" ),
			PhysThing( CRATE_FRICTION, CRATE_DENSITY )
		{
			int value = (int) Math::rangeRandom(1, 5);
			float sideY = (value == 4) ? (CRATE_SIDE * 2.f) : CRATE_SIDE;

			static const Color colorValue [] = {
				Color::BLACK,
				Color::WHITE,
				Color::RED,
				Color::GREEN,
				Color::WHITE
			};

			static const float densityValue [] = {
				1,
				0.25f,
				0.5f,
				0.8f,
				0.25f //twice as big
			};

			color = colorValue[value];
			density = densityValue[value];

			setSize(CRATE_SIDE, sideY);
			scale = Dojo::Vector(getSize().x, getSize().y);

			initPhysics(this, PG_CRATES);

			setShader(getGameState()->getShader("normal"));

			setTexture(getGameState()->getTexture("crate"), 0);
			setTexture(getGameState()->getTexture("crateNormal"), 1);
		}

		virtual void onAction(float dt) override {
			updatePhysics();
			Dojo::Renderable::onAction(dt);
		}

	protected:
		int value;
	};
}
 
#endif // Crate_h__