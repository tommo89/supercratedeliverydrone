#ifndef FrontierWorlds_h__
#define FrontierWorlds_h__

#include "common_header.h"

#include "Level.h"

namespace Drone
{
	class DroneGame : 
		public Dojo::Game,
		public Dojo::ResourceGroup
	{
	public:

		DroneGame() :
		Game( "SUPER CRATE DELIVERY DRONE 2", 0, 0, Dojo::DO_LANDSCAPE_RIGHT, 1.f/30.f )
		{

		}

		void reset();

	protected:

		virtual void onBegin();

		virtual void onLoop( float dt )
		{

		}
		virtual void onEnd()
		{

		}

		//----- immediate substate events
		virtual void onStateBegin()
		{

		}

		virtual void onStateLoop( float dt )
		{

		}

		virtual void onStateEnd()
		{

		}
		
	protected:
	private:
	};
}

#endif // Gibberish_h__