#ifndef Dronething_h__
#define Dronething_h__

#include "common_header.h"

#include "Physthing.h"

#include "Crate.h"

#define DEAD_ZONE 0.2f

#define PROPELLER_DENSITY 1.f
#define PROPELLER_DIAMETER 0.3f	  
#define PROPELLER_WIDTH 0.07f
#define PROPELLER_OFFSET 0.13f
#define PROPELLER_TURN_IMPULSE 4.f
#define PROPELLER_FORCE 2.5f

#define DRONE_DIAMETER 0.6f
#define DRONE_HEIGHT 0.2f
#define DRONE_FRICTION 0.7f
#define DRONE_DENSITY 1.f
#define DRONE_STABILIZATION 0.7f
#define DRONE_CORRECTION_MIN 15.f
#define DRONE_CORRECTION_MAX 75.f
#define DRONE_RESISTANCE 4.f

#define CLAW_LENGTH 0.25f
#define CLAW_WIDTH 0.1f
#define CLAW_OFFSET (DRONE_DIAMETER / 1.9f)
#define CLAW_DENSITY DRONE_DENSITY
#define CLAW_RETAIN_FORCE (-2.5f)
#define CLAW_CLOSE_FORCE 8.5f

namespace Drone {
	class Dronething : 
		public PhysThing,
		public Dojo::Renderable, 
		public Dojo::InputDevice::Listener 
	{
		class Dust :
		public Renderable {
		public :

			Dust(Object* parent, const Vector& pos, const Vector& direction ) :
				Renderable(parent, pos, "texturedQuad" ),
				totalLifetime(1),
				lifetime(0)
			{

				scale = Math::rangeRandom(0.03, 0.1);

				speed = -direction;

				float off = Math::rangeRandom(-PROPELLER_DIAMETER / 2.f, PROPELLER_DIAMETER/2.f);

				position.x += off * direction.y;
				position.y -= off * direction.x;

				setRotation(Vector( 0,0, Math::rangeRandom(0, 360) ));
			}

			virtual void onAction(float dt) {
				lifetime += dt;

				speed *= 0.9;

				color.a = 1.f - lifetime / totalLifetime;

				if (lifetime > totalLifetime)
					dispose = true;
					
				Renderable::onAction(dt);
			}

			float lifetime, totalLifetime;
		};

		class PropellerThing :
			public PhysThing,
			public Dojo::Renderable 
		{
		public: 
			PropellerThing(Dronething* parent, bool left) :
				PhysThing( DRONE_FRICTION, PROPELLER_DENSITY )	,
			Renderable( 
				parent, 
				parent->position + Vector(0, PROPELLER_OFFSET ) + Vector((DRONE_DIAMETER / 2.f + PROPELLER_DIAMETER / 2.f) * (left ? -1 : 1), 0), 
				"texturedQuad" ) ,
				drone(parent),
				multiplier(1),
				timer(0),
				thrust(0)
			{
				cullMode = CullMode::CM_DISABLED;

// 				setTexture(getGameState()->getTexture("shipPropeller"), 0);
// 				setTexture(getGameState()->getTexture("shipPropellerNormal"), 1);
// 				setShader(getGameState()->getShader("normal"));

				setSize(PROPELLER_DIAMETER, PROPELLER_WIDTH);	
				scale = Vector(getSize().x, getSize().y) * 2;

				initPhysics(this, parent);

				auto parentBody = parent->getBody();

				jointDef.Initialize(parentBody, body, body->GetWorldCenter());

				jointDef.lowerAngle = -0.25f * b2_pi; // -90 degrees
				jointDef.upperAngle = 0.25f * b2_pi; // 45 degrees
				jointDef.enableLimit = true;

				jointDef.maxMotorTorque = 10.0f;
				jointDef.motorSpeed = 0.0f;
				jointDef.enableMotor = true;

				auto box2D = ((Level*) graphics->getGameState())->getBox2D();
				joint = (b2RevoluteJoint*)box2D->CreateJoint(&jointDef);
			}

			virtual void onAction(float dt) override {
				updatePhysics();

				timer += thrust * dt * 10.f;
				scale.x = getSize().x * sinf(timer);

				if (!joint)
					thrust *= 0.9; //gnik gnik gnik

				Dojo::Renderable::onAction(dt);
			}

			virtual void onCorrection(float mult) {
				multiplier = Math::clamp(mult, 1.f, 0.f);
			}

			virtual void onPower(float state, float brake) {
				
				//dead zone, renormalize
				if (std::abs(state) < DEAD_ZONE)
					return;

				state = (std::abs(state) - DEAD_ZONE) / (1.f - DEAD_ZONE) * Math::sign(state);

				float throttle = 1.f - brake;

				thrust = Math::lerp(PROPELLER_FORCE * throttle, hoveringForce * throttle, powf(state, 10));
				thrust *= Math::sign(state);

				//get the direction of the body	
				b2Vec2 F(0, thrust * multiplier);

				b2Vec2 wF = body->GetWorldVector(F);

				body->ApplyForceToCenter(wF, true);

				//spawn particles
				if (Math::random() < wF.Length() * 0.3)
					getGameState()->addChild(new Dust(this, position, Vector(wF.x, wF.y).normalized()), getLayer()-1);
			}

			virtual void onSteer(float state) {

				float f = (-state + 1.f) * 0.5f;
				float targetAngle = jointDef.lowerAngle + (jointDef.upperAngle - jointDef.lowerAngle) * f;

				float M = (targetAngle - (body->GetAngle() - drone->getBody()->GetAngle())) * PROPELLER_TURN_IMPULSE;

				joint->SetMotorSpeed(M);
			}

			void _notifyTotalWeightForce(float f) {
				hoveringForce = f / 1.999f;
			}

			virtual void onBreak() {
				auto box2D = ((Level*) graphics->getGameState())->getBox2D();
				box2D->DestroyJoint(joint);

				joint = nullptr;
			}

			virtual void onCollision(PhysThing* other, float force) override
			{
				drone->onCollision(other, force);
			}

			virtual float getMass() const override {
				return drone->getMass();
			}

		protected:
			Dronething* drone;
			float multiplier;
			float hoveringForce;
			float timer;
			float thrust;

			b2RevoluteJointDef jointDef;
			b2RevoluteJoint* joint;
		};

		class ClawThing : 
			public PhysThing  ,
			public Renderable
		{
		public:
			ClawThing(Dronething* parent, bool left) :
			PhysThing(1.0, CLAW_DENSITY),
			Renderable(
			parent,
			parent->position + Vector(0, -CLAW_LENGTH * 0.9f) + Vector(CLAW_OFFSET * (left ? -1 : 1), 0),
			"texturedQuad"),
			drone(parent),
			left(left)
			{

				setSize(CLAW_WIDTH, CLAW_LENGTH);
				scale = Vector(getSize().x, getSize().y);

				float w = getHalfSize().x;
				float h = getHalfSize().y;
				float o1 =  CLAW_WIDTH * (left ? -0.8 : 0.8);
				float o2 = o1 * 0.5f;

				initPhysics(this, parent);

				auto parentBody = parent->getBody();

				b2Vec2 worldHinge;
				worldHinge.x = position.x;
				worldHinge.y = position.y + getHalfSize().y;

				jointDef.Initialize(parentBody, body, worldHinge);
// 
				if (left)
				{
					jointDef.lowerAngle = -75 * Math::EULER_TO_RADIANS;
					jointDef.upperAngle = 5 * Math::EULER_TO_RADIANS;
				}
				else
				{
					jointDef.lowerAngle = -5 * Math::EULER_TO_RADIANS;
					jointDef.upperAngle = 75 * Math::EULER_TO_RADIANS;
				}

				jointDef.enableLimit = true;
// 
				jointDef.maxMotorTorque = 9.f;
				jointDef.motorSpeed = 0.0f;
				jointDef.enableMotor = true;

				jointDef.collideConnected = false;

				auto box2D = ((Level*) graphics->getGameState())->getBox2D();
				joint = (b2RevoluteJoint*) box2D->CreateJoint(&jointDef);

			}

			virtual void onGrab( bool close ) 
			{
				float M = close ? CLAW_CLOSE_FORCE : CLAW_RETAIN_FORCE;
				joint->SetMotorSpeed(left ? M : -M);
 			}

			virtual void onAction(float dt) override
			{
				updatePhysics();

				Dojo::Renderable::onAction(dt);
			}

			virtual void onBreak() {
				auto box2D = ((Level*) graphics->getGameState())->getBox2D();
				box2D->DestroyJoint(joint);

				joint = nullptr;
			}

			virtual void onCollision(PhysThing* other, float force) override
			{
				if ( other && other->getGroup() == PG_CRATES )
					collidedCrates.insert(other);

				drone->onCollision(other, force);
			}

			virtual float getMass() const override {
				return drone->getMass();
			}

			std::unordered_set< PhysThing*>& getCollidedCrates()
			{
				return collidedCrates;
			}

		protected:
			bool left;
			Dronething* drone;

			std::unordered_set< PhysThing*> collidedCrates;

			b2RevoluteJointDef jointDef;
			b2RevoluteJoint* joint;
		};

	public:

		Dronething( Dojo::Object* parent, const Dojo::Vector& pos, Dojo::InputDevice* input, int ID ) :
			PhysThing( DRONE_FRICTION, DRONE_DENSITY ),
			Renderable(parent, pos, "texturedQuad" ),
			input(input),
			lastAngularVelocity(0),
			broken(false),
			savedForce(0)
		{
			setSize( DRONE_DIAMETER, DRONE_HEIGHT );
			scale = Dojo::Vector(getSize().x, getSize().y);

			input->addListener(this);


			initPhysics(this, (PhysicGroup) (PhysicGroup::PG_DRONE + ID));

			totalMass = body->GetMass();

			_createPropeller(0);
			_createPropeller(1);

			_createClaw(0);
			_createClaw(1);

			totalMass *= body->GetWorld()->GetGravity().Length(); //by g

			propellers[0]->_notifyTotalWeightForce(totalMass);
			propellers[1]->_notifyTotalWeightForce(totalMass);

			body->SetLinearVelocity(b2Vec2(0,0));
		}

		virtual ~Dronething() 
		{
			if (input)
				input->removeListener(this);
		}

		///ButtonPressed events are sent when the button bound to "action" is pressed on the device j
		virtual void onButtonPressed(Dojo::InputDevice* j, int action)	
		{
		
		}
		///ButtonReleased events are sent when the button bound to "action" is released on the device j
		virtual void onButtonReleased(Dojo::InputDevice* j, int action)
		{
		
		}

		///AxisMoved events are sent when the axis a is changed on the device j, with a current state of "state" and with the reported relative speed
		virtual void onAxisMoved(Dojo::InputDevice* j, Dojo::InputDevice::Axis a, float state, float speed)	
		{
			
		}

		///this event is fired just before the device is disconnected and the InputDevice object deleted
		virtual void onDisconnected(Dojo::InputDevice* j)
		{
			input = nullptr;
		}

		virtual void onCollision(PhysThing* other, float force) override
		{
			if ( (!other || other->getGroup() != group) )
				collisionDamageMap[other].push_back( force );
		}

		virtual float getMass() const override {
			return totalMass;
		}

		virtual void onAction(float dt) override {

			if (!broken ) 
			{		
				//check for collided crates
				carried = nullptr;
				for (auto c : claws[0]->getCollidedCrates())
				{
					if (claws[1]->getCollidedCrates().count(c))
						carried = c;
				}
				claws[0]->getCollidedCrates().clear();
				claws[1]->getCollidedCrates().clear();

				float receivedForce = 0;
				for (auto& entry : collisionDamageMap)
				{
					if ( entry.first == carried && carried )
						continue;

					float force = 0;
					for (auto f : entry.second)
						force += f;

					receivedForce += force / (float)entry.second.size();
				}
				collisionDamageMap.clear();
				carriedDetectionSet.clear();

				if (receivedForce > DRONE_RESISTANCE)
				{
					broken = true;
					propellers[0]->onBreak();
					propellers[1]->onBreak();
					claws[0]->onBreak();
					claws[1]->onBreak();
				}
				else
				{
					savedForce = std::max(receivedForce, savedForce - dt * 3.f);
					color.b = color.g = 1.f-(savedForce/DRONE_RESISTANCE);

					float x = lastAngularVelocity = Math::lerp(lastAngularVelocity, body->GetAngularVelocity(), 0.5f);
					float a = Math::clamp(std::abs(x * Math::RADIANS_TO_EULER), DRONE_CORRECTION_MAX, DRONE_CORRECTION_MIN);
					float f = (a - DRONE_CORRECTION_MIN) / (DRONE_CORRECTION_MAX - DRONE_CORRECTION_MIN);

					float correction = f * Math::sign(-x);

					if (x < 0) {
						propellers[0]->onCorrection(1.f - correction);
						propellers[1]->onCorrection(1);
					}
					else {
						propellers[1]->onCorrection(1.f + correction);
						propellers[0]->onCorrection(1);
					}

					propellers[0]->onPower(input->getAxis(InputDevice::AI_LY), input->getAxis(InputDevice::AI_SLIDER1));
					propellers[1]->onPower(input->getAxis(InputDevice::AI_RY), input->getAxis(InputDevice::AI_SLIDER2));

					propellers[0]->onSteer(input->getAxis(InputDevice::AI_LX));
					propellers[1]->onSteer(input->getAxis(InputDevice::AI_RX));

					claws[0]->onGrab(input->isKeyDown(KC_JOYPAD_9));
					claws[1]->onGrab(input->isKeyDown(KC_JOYPAD_10));
				}
				
			}

			updatePhysics();

			Dojo::Renderable::onAction(dt);
		}

	protected:
		bool broken;

		std::unordered_map< PhysThing*, std::vector<float> > collisionDamageMap;
		std::unordered_set<PhysThing*> carriedDetectionSet;

		float lastAngularVelocity;
		float totalMass;
		float savedForce;
		Dojo::InputDevice* input;
		PhysThing* carried;

		PropellerThing* propellers[2];
		ClawThing* claws[2];

		void _createPropeller(int i) {

			propellers[i] = new PropellerThing(this, i == 0);

			getGameState()->addChild(propellers[i], LL_DRONE_PARTS);

			totalMass += propellers[i]->getBody()->GetMass();
		}

		void _createClaw(int i) {
			claws[i] = new ClawThing(this, i == 0);

			getGameState()->addChild(claws[i], LL_DRONE_PARTS);

			totalMass += claws[i]->getBody()->GetMass();
		}
	};
}

#endif // Dronething_h__
