
#ifndef Level_h__
#define Level_h__
        
#include "common_header.h"

namespace Drone {
    
    class DroneGame;
	class Dronething;
    
    class Level :
	public Dojo::GameState,
	public Dojo::InputDevice::Listener,
	public Dojo::InputSystem::Listener,
	public Dojo::TouchArea::Listener,
	public b2ContactListener
    {
    public:

        
        Level( DroneGame* game );

		b2World* getBox2D() const {
			return box2D.get();
		}

		virtual void onDeviceConnected(Dojo::InputDevice* j) override;
		void onDeviceDisconnected(Dojo::InputDevice* j) override;

		void onButtonReleased(Dojo::InputDevice* j, int action) override;
	
		virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
    protected:

		std::unordered_set<Dronething*> drones;
		std::unordered_set<InputDevice*> inactiveDevices;
        
        virtual void onBegin();
        virtual void onEnd();
        
        //----- immediate substate events
        virtual void onStateBegin();
        
        virtual void onStateLoop( float dt );
        
        virtual void onStateEnd();

		std::unique_ptr<b2World> box2D;

		void _spawnCrates(float startX);



    private:
    };
}   
#endif // Level_h__