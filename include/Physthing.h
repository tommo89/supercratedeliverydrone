#ifndef Header_h__
#define Header_h__

#include "common_header.h"

namespace Drone {

	class PhysThing
	{
	public:

		PhysThing( float friction, float density ) :
		friction( friction ),
		density(density)
		{

		}

		virtual float getMass() const
		{
			return body->GetMass();
		}

		void addShape( float friction, float density, std::vector<b2Vec2>* points = nullptr, bool sensor = false )
		{
			b2PolygonShape dynamicBox;

			if (!points)
				dynamicBox.SetAsBox(graphics->getHalfSize().x, graphics->getHalfSize().y);
			else
				dynamicBox.Set(points->data(), points->size());

			b2FixtureDef fixtureDef;

			fixtureDef.shape = &dynamicBox;
			fixtureDef.density = density;
			fixtureDef.friction = friction;
			fixtureDef.filter = b2Filter();
			fixtureDef.filter.groupIndex = group;
			fixtureDef.isSensor = sensor;

			fixtureDef.userData = this;

			body->CreateFixture(&fixtureDef);
		}

		void initPhysics(Dojo::Renderable* graphics, PhysicGroup group, std::vector<b2Vec2>* points = nullptr );
		void initPhysics(Dojo::Renderable* graphics, PhysThing* partOf = nullptr, bool fixed = false, std::vector<b2Vec2>* points = nullptr);

		PhysicGroup getGroup() const {
			return group;
		}

		b2Body* getBody() const {
			return body;
		}

		virtual void onCollision( PhysThing* other, float force ) 
		{
		}

		void updatePhysics();

	protected:

		float friction, density;

		Dojo::Renderable* graphics;

		b2Body* body;
		PhysicGroup group;
	private:
	};

}

#endif // Header_h__
