
#ifndef common_header_h__
#define common_header_h__

#include <dojo.h>

#include <Box2D/Box2D.h>

#include <memory>
#include <unordered_set>
#include <unordered_map>

namespace Drone {

	enum PhysicGroup {
		PG_BACKGROUND = 1,
		PG_CRATES,
		PG_DRONE,

		PG_DRONE_MAX = PG_DRONE + 8
	};

	enum Keys {
		KEY_RESET = 999	,
		KEY_JOIN
	};


	enum Layers
	{
		LL_BACKGROUND,

		LL_CLOUDS,
		LL_CLOUDS_MIDDLE,

		LL_CITY,

		LL_GROUD,

		LL_DRONES,
		LL_DRONE_PARTS,

		LL_CLOUDS_FRONT,

		LL_GUI,

		LL_FADER,

		LL_DEBUG
	};
}

using namespace Dojo;

#endif // common_header_h__