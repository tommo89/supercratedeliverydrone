#include "stdafx.h"

#include "DroneGame.h"

#include "Level.h"

using namespace Drone;
using namespace Dojo;


void DroneGame::onBegin()
{
	//load common res
	addPrefabMeshes();
	addFolder("data");
	loadResources();

	setState(new Level(this));
}

void DroneGame::reset() {
	setState(new Level(this));
}
