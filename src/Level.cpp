#include "stdafx.h"

#include "Level.h"

#include "DroneGame.h"
#include "Dronething.h"
#include "Crate.h"
#include "Cloud.h"

using namespace Drone;
using namespace Dojo;

Level::Level(DroneGame* game) :
	GameState( game )
{

}

void Level::_spawnCrates(float startX)
{
	float x = startX;
	for (int i = 0; i < 6; ++i) {

		int n = (int) Math::rangeRandom(1, 5);
		float y = 0.5;
		for (int j = 0; j < n; ++j) {
			auto c = new Crate(this, Vector(x, y));
			addChild(c, LL_DRONES);

			y += c->getSize().y + 0.01;
		}
		x += CRATE_SIDE + 0.2;
	}
}

void Level::onBegin()
{
	addSubgroup((DroneGame*) getGame());

	loadResources();

	{
		//viewport
		float w = (float) Platform::getSingleton()->getWindowWidth();
		float r = w / (float)Platform::getSingleton()->getWindowHeight();

		Vector worldSize( 11*r, 11 );
		//Vector targetSize( 2048, 2048/r );
		Vector targetSize( 1800*r, 1800 );

		camera = new Viewport(this,
			Vector(0,0,1), 
			worldSize,
			Color(85.f/255, 217.f/255, 1.f, 1.f), 
			70, 0.01, 1000 );

		camera->setTargetSize( targetSize );

		addChild( camera );
		setViewport( camera );
	}

	Platform::getSingleton()->getInput()->addListener(this);

	{
		b2Vec2 gravity(0.0f, -8.0f);
		box2D = std::unique_ptr<b2World>(new b2World(gravity));
		box2D->SetContactListener(this);

		b2BodyDef groundBodyDef;

		groundBodyDef.position.Set(0, 0);	
		b2Body* groundBody = box2D->CreateBody(&groundBodyDef);

		float x = -100;
		for (int i = 0; i < 100; ++i) {

			Sprite* ground = new Sprite(this, Vector(x, 0.f), "ground", -1.f, true );
			ground->position.y -= ground->getHalfSize().y;
			addChild(ground, LL_GROUD);


			Sprite* city = new Sprite(this, Vector(x, 1.2), "citySkyline", -1.f, true);
			city->setFrame((int) Math::rangeRandom(0, city->getFrameSet()->getFrameNumber()) );
			addChild(city, LL_CITY);

			x += ground->getSize().x;

			float h = ground->getHalfSize().y;
			float w = ground->getHalfSize().x;

			b2Vec2 vertices [] = {
				b2Vec2(-w + x, -h - h),
				b2Vec2(w + x, -h - h),
				b2Vec2(w + x, h - h),
				b2Vec2(-w + x, h - h),
			};

			b2PolygonShape groundBox;
			groundBox.Set(vertices, 4);

			b2FixtureDef def;
			def.filter.groupIndex = PG_BACKGROUND;
			def.shape = &groundBox;
			def.density = 2;

			groundBody->CreateFixture(&def);
		}	

		//make crates
		_spawnCrates(-9);
		_spawnCrates(5);

		//make clouds
		for (int i = 0; i < 15; ++i) {
			Vector pos = camera->position + Math::randomVector2D(-8, 8, -1);

			new Cloud(this, pos); //yes it is horrible but it is assigning itself
		}
	}		

	//add all existing devices 
	auto& devices = Platform::getSingleton()->getInput()->getDeviceList();

	for (auto d : devices)
		onDeviceConnected(d);
}

void Level::onEnd()
{
	Platform::getSingleton()->getRender()->removeAllRenderables();
	Platform::getSingleton()->getRender()->removeAllViewports();
	Platform::getSingleton()->getInput()->removeListener(this);

	for (auto d : Platform::getSingleton()->getInput()->getDeviceList() )
		d->removeListener(this);
}

//----- immediate substate events
void Level::onStateBegin()
{

}

void Level::onStateLoop( float dt )
{
	int32 velocityIterations = 16;
	int32 positionIterations = 12;

	box2D->Step(getGame()->getNativeFrameLength(), velocityIterations, positionIterations);	

	//center the camera on drones
	Vector center;
	if (drones.size() > 0) {
		
		//find also the minimum AABB containing all the players
		Vector max = Vector::MIN, min = Vector::MAX;

		for (auto drone : drones)
		{
			center += drone->position;

			max = Math::max(drone->position, max);
			min = Math::min(drone->position, min);
		}

		max += 2.f;
		min -= 2.f;
		center *= 1.f / drones.size();

		//try to fit the AABb in the view
		if (drones.size() > 1)
		{
			float w = (float) Platform::getSingleton()->getWindowWidth();
			float r = w / (float) Platform::getSingleton()->getWindowHeight();
			
			Vector minViewSize(11 * r, 11);
			Vector size = Math::max(max - min, minViewSize);

			float br = size.x / size.y;

			if (br < r) // too tall
				size.x = size.y * r;
			else if (br > r) //too squashed
				size.y = size.x / r;

			camera->setSize(size);
			camera->setTargetSize(Vector(1800 * r, 1800) * (size.x / minViewSize.x));
		}
	}

	//look up
	center -= Vector::NEGATIVE_UNIT_Y * 2;

	float k = 7.1;
	camera->speed = (center - camera->position)	* k;

	Color baseColor(85.f / 255, 217.f / 255, 1.f, 1.f);

	camera->setClearColor(baseColor.lerp(camera->position.y / 1000.f, Color::BLACK));
}

void Level::onStateEnd()
{

}

void Level::onButtonReleased(Dojo::InputDevice* j, int action) {
	if (action == KEY_RESET)
		((DroneGame*) getGame())->reset();

	else if (action == KEY_JOIN) {

		if (inactiveDevices.count(j)) {
			Vector pos = Vector(-3.f + drones.size() * 2.f, 1, 0);
			auto drone = new Dronething(this, pos, j, drones.size());

			addChild(drone, LL_DRONES);

			drones.insert(drone);

			inactiveDevices.erase(j);
		}
	}
}

void Level::onDeviceConnected(Dojo::InputDevice* j) {

	j->addBinding(KEY_RESET, KC_R);
	j->addBinding(KEY_JOIN, KC_JOYPAD_5);
	j->addBinding(KEY_RESET, KC_JOYPAD_6);

	j->addListener(this);
	inactiveDevices.insert(j);
}

void Level::onDeviceDisconnected(Dojo::InputDevice* j) {
	inactiveDevices.erase(j);
}

void Drone::Level::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
	static b2WorldManifold worldManifold;

	auto phA = (PhysThing*)contact->GetFixtureA()->GetUserData();
	auto phB = (PhysThing*) contact->GetFixtureB()->GetUserData();

	if (contact->GetFixtureA()->GetFilterData().groupIndex == contact->GetFixtureB()->GetFilterData().groupIndex)
		return;

	contact->GetWorldManifold(&worldManifold);

	const b2Body* bodyA = contact->GetFixtureA()->GetBody();
	const b2Body* bodyB = contact->GetFixtureB()->GetBody();

	b2Vec2 point = worldManifold.points[0];

	b2Vec2 vA(0, 0), vB(0, 0);

	if(phA)
	{
		vA = bodyA->GetLinearVelocityFromWorldPoint(point);
		vA.x *= phA->getMass();
		vA.y *= phA->getMass();
	}

	if (phB)
	{
		vB = bodyB->GetLinearVelocityFromWorldPoint(point);
		vB.x *= phB->getMass();
		vB.y *= phB->getMass();
	}

	float32 force = b2Dot(vB - vA, worldManifold.normal);

	if (phA)
		phA->onCollision(phB, force);

	if (phB)
		phB->onCollision(phA, force);
}
