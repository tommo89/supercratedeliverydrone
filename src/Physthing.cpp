#include "stdafx.h"

#include "Physthing.h"
#include "Level.h"

using namespace Drone;
using namespace Dojo;

void PhysThing::initPhysics(Dojo::Renderable* g, PhysicGroup physGroup, std::vector<b2Vec2>* points) {

	graphics = g;
	group = physGroup;

	auto box2D = ((Level*) graphics->getGameState())->getBox2D();

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;

	bodyDef.position.Set( g->position.x, g->position.y );
	bodyDef.angularDamping = 0.1;
	bodyDef.linearDamping = 0.1;
	bodyDef.bullet = true;

	body = box2D->CreateBody(&bodyDef);

	addShape(friction, density, points, false);
}

void PhysThing::initPhysics(Dojo::Renderable* graphics, PhysThing* partOf /* = nullptr */, bool fixed /* = false */, std::vector<b2Vec2>* points) {

	if (!fixed) {
		initPhysics(graphics, partOf->getGroup(), points );
	}

	else {

	}
}

void PhysThing::updatePhysics() {

	b2Transform t = body->GetTransform();

	graphics->position.x = t.p.x;
	graphics->position.y = t.p.y;

	graphics->setRotation(Vector(0, 0, t.q.GetAngle()));
}