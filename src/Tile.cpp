#include "stdafx.h"

#include "Tile.h"

#include "TileManager.h"


using namespace Dojo;
using namespace FW;

Tile::Tile( int ID, const Dojo::Table& desc, TileManager* creator ) :
ID( ID ),
mDesc( desc ),
pCreator( creator ),
mModelSet( nullptr ),
mSoundSet( nullptr ),
mTileSet( nullptr ),
pDrops( nullptr ),
pPlaces( nullptr ),
mContactDamageType( DT_NONE )
{

}

bool Tile::onLoad() {
	if (mDesc.getBool("has_model"))
		mModelSet = pCreator->getModelSet(mDesc.getName());

	mTileSet = pCreator->getTerrainTileSet(mDesc.getString("terrainSet", mDesc.getName()));

	mSoundSet = pCreator->getMaterialSoundSet(mDesc.getString("soundSet", mDesc.getName()));

	mHasInside = mDesc.getBool("has_inside");
	mAdaptive = mDesc.getBool("model_adaptive");

	pDrops = pCreator->getTile(mDesc.getString("drops", mDesc.getName()));

	if (mDesc.exists("places"))
		pPlaces = pCreator->getTile(mDesc.getString("places"));

	//grab all the damage multipliers
	static const String damageTypeNameMap [] = {
		"none", //DT_NONE,
		"burn", //DT_BURN,
		"acid", //DT_ACID,
		"freeze", //DT_FREEZE,
		"cut", //DT_CUT,
		"crash", //DT_CRASH,
		"blast", //DT_BLAST,
		"shock", //DT_SHOCK,
		"dig",	//DT_DIG
	};
	
	int i = 0;
	for (auto& name : damageTypeNameMap)
		mDamageMult[i++] = mDesc.getNumber(name);

	if (mDesc.exists("damage_type")) {
		i = 0;
		auto & damageName = mDesc.getString("damage_type");
		for (auto& name : damageTypeNameMap) {
			if (name == damageName) {
				mContactDamageType = (DamageType) i;
				break;
			}
		}
	}

	mNoDropTier = mDesc.getNumber("nodrop_tool_tier_less_than");
	mNoDropMult = mDesc.getNumber("nodrop_tool_mult_less_than");

	mSolidityMult = mDesc.getNumber("solidity", 1);

	mEmissive = mDesc.getNumber("emissive");
	mTransparent = mDesc.getNumber("transparent");
	mConductive = mDesc.getNumber("conductive");

	mFaceBoundMax = mDesc.getNumber("faceBoundMax");
	mFaceBoundMin = mDesc.getNumber("faceBoundMin", -1); //can be nonexisting

	mTileHeight = mDesc.getNumber("tileHeight",1);

	//liquid stuff, if liquid
	if (mLiquid = mDesc.getBool("liquid"))
	{
		mLiquidSpreadMult = mDesc.getNumber("liquid_spread");
		
		pFreezingResult = pCreator->getTile(mDesc.getString("freezes"));

		//TODO make a better structure for fluids
		/*for (auto& entry : *mDesc.getTable("liquid_freeze"))
			mLiquidSolidify.push_back(pCreator->getTile(entry.second->getAsString()));

		for (auto & entry : *mDesc.getTable("liquid_drain"))
			mLiquidDrain.push_back(pCreator->getTile(entry.second->getAsString()));	  */
	}

	return true;
}