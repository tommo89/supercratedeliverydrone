#include "stdafx.h"

#include "TileManager.h"

#include "Tile.h"

#include "ModelSet.h"
#include "MaterialSoundSet.h"
#include "TerrainTileSet.h"

using namespace Dojo;
using namespace FW;

TileManager::TileManager(const Dojo::Table& desc) {

	//load all the resource pls

	//create and map all the tiles
	auto& idMap = *desc.getTable("ID_Map");

	for (auto& entry : *desc.getTable("definitions"))
	{
		int id = idMap.getNumber(entry.first);			  //get the id for this tile

		auto t = new Tile(id, *entry.second->getAsTable(), this);

		mTiles[entry.first] = std::unique_ptr< Tile >(t);
	}

	//load all the tiles
	for (auto& entry : mTiles)
		entry.second->onLoad();
}