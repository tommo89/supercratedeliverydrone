#include "stdafx.h"

#include "DroneGame.h"

using namespace Dojo;
using namespace Drone;

int main( int argc, char** argv )
{
	Table config;

	Platform* platform = Platform::create( config );

	platform->run( new DroneGame() );

	return 0;
}

#ifdef WIN32

INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
{
	main( 1, &strCmdLine );
}

#endif